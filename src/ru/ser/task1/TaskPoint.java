package ru.ser.task1;

import java.util.Scanner;

public class TaskPoint {
   private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символы - ");
        String text = sc.nextLine();
        int i = text.indexOf(".");
        System.out.println("Символов до точки - " +i);
        String space = text.split("\\.", 2)[0];
        System.out.print("Количество пробелов = ");
        System.out.println(space.length() - space.replaceAll(" ", "").length());
    }
}
