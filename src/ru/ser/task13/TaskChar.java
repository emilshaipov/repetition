package ru.ser.task13;

import java.util.Scanner;

public class TaskChar {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите символ(ы): ");
        String string = sc.nextLine();
        char[] character = string.toCharArray();//создаем массив символов в строке

        for (int i = 0; i < character.length; i++) {
            if ((character[i] >= 'A' && character[i] <= 'Z') ||
                    (character[i] >= 'a' && character[i] <= 'z') ||
                    (character[i] >= 'А' && character[i] <= 'Я') ||
                    (character[i] >= 'а' && character[i] <= 'я')) {
                System.out.println(character[i] + " - Это буква!");
            }
            if (character[i] >= '0' && character[i] <= '9') {
                System.out.println(character[i] + " - Это цифра!");
            }
            if (character[i] == ',' || character[i] == '.' || character[i] == '!' ||
                    character[i] == '?' || character[i] == ';' || character[i] == ':') {
                System.out.println(character[i] + " - Это знак пунктуации!");
            }
        }
    }
}
