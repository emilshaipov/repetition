package ru.ser.task11;

import java.util.Scanner;

public class TaskEleven {
    public static void main(String[] args) {
        System.out.print("Введите кол-во дней: ");
        int days=new Scanner(System.in).nextInt();

        convert(days);

    }
    private static void convert(int days) {
        if(days<=0) throw new IllegalArgumentException();
        System.out.println("В "+ days+" днях: "+ days*24 + " часов, "+ days*1440 +" минут, "+ days*86400+" секунд.");

    }
}
