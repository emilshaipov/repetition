package ru.ser.task7;

import java.util.Scanner;

public class TaskSeven {


    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double number = new Scanner(System.in).nextDouble();

        isInteger(number);

    }

    static void isInteger(double number) {
        if (number % 1 == 0) System.out.println("Это целое число");
        else System.out.println("Это число не является целым");

    }
}
