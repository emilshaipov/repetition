package ru.ser.task5;

public class TaskFive {

    public static void main(String[] args) {
        int x, y;
        for (x = 2; x < 101; x++) {
            y = 0;
            for (int i = 2; i <= x; i++) {
                if (x % i == 0)
                    y++;
            }
            if (y <= 2)
                System.out.println(x + " простое число");
        }
    }
}